#!/bin/bash
#===============
# Description : This script provides a way to post shaarlinks to a Diaspora* pod
# Author : Kevin Merigot (http://www.mypersonnaldata.eu/contact.html)
# Version : 1.2
# Licence : Creative Commons By-SA 4.0 (http://creativecommons.org/licenses/by-sa/4.0/)
# This script is provided without any warranty
#===============

#=====
# FEATURES
#=====
# - Post new links to a Diaspora* pod
# - Exclude some tags from being posted (if you use them for another purpose for instance)
# - Available in different colors :p
#=====

#=====
# INSTALL
#=====
# This script requires :
#- cliaspora : https://freeshell.de/~mk/projects/cliaspora.html
#- xmlstarlet (you can install it with a simple apt-get install xmlstarlet or yum install xmlstarlet)
#
# 1) Install this script somewhere you want (like in /opt/shaar2diasp)
# 2) Add a cron like (for a 15 minutes refresh) :
# */15 * * * * myuser /opt/shaar2diasp/shaar2diasp.sh
# 3) Enjoy
#=====

##=====
# Configuration
##=====
# URL of shaarli RSS feed
RSS_URL="http://wwww.mywebsite.tld/shaarli/?do=rss&searchtags=fs"
# handle to log in (e.g. username@framasphere.org)
LOGIN="login"
# the password, of course
PASSWORD="password"
# the aspect you want to post into
ASPECT="public"
# the tmp file, where the raw data will go
TMPFILE=`mktemp /tmp/shaar2diasp-XXXXXX.tmp`
# the file where you keep a trace of what has been posted
# (you should not delete it at all)
SEENFILE=/opt/shaar2diasp/shaar.seen
# the tags you want to exclude from your posts
EXCLUDEDTAGS=("fs" "test");

##=====
# Now, the tough thing
##=====

# we retrieve some usefull values
##=====
## Try to determine binaries path or exit
##=====
CLIASPORABIN=`type cliaspora 2>/dev/null`
if [ "x$CLIASOPABIN" = "x" ]
then
	echo "No cliaspora in $PATH, exiting"
	exit 3
fi

WGETBIN=`type wget 2>/dev/null`
if [ "x$WGETBIN" = "x" ]
then
	echo "No wget in $PATH, exiting"
	exit 3
fi

XMLSTARLETBIN=`type xmlstarlet 2>/dev/null`
if [ "x$XMLSTARLETBIN" = "x" ]
then
	echo "No xmlstarlet in $PATH, exiting"
	exit 3
fi

# if we don't have any cliaspora installed, we won't go any further
if [[ $CLIASPORABIN = "" ]]
then
	echo "cliaspora not installed, exiting..."
	exit 1
fi

# Connecting to the pod
$CLIASPORABIN session new $LOGIN $PASSWORD

# if the connection fails, well... we won't go any further neither
if [[ $? -ne 0 ]]
then
	echo "Unable to connect, exiting..."
	exit 2
fi

# check if a tag is excluded
# $1 : tag to check against $EXCLUDEDTAGS
function is_excluded_tag()
{
	for value in ${EXCLUDEDTAGS[@]}
	do
        if [ "$value" == "$1" ] ; then
			return 1
		fi
	done
	return 0
}

# send built item to diaspora
# if you want to change the way your links are posted, you should take a look to this function then
# $1 : title
# $2 : link
# $3 : description
# $4 : tags
function senditem() {
echo "[<h3>$1</h3>]($2)\
$3\
$4" | $CLIASPORABIN post $ASPECT
}

# we retrieve and filter data

$WGETBIN ${RSS_URL} -O - 2>/dev/null | $XMLSTARLETBIN sel -T -t -m "/rss/channel/item" -o "<BEGINITEM>" -n -v "guid" -n -v "title" -n -v "link" -n -o "<BEGINTAGS>" -n -v "category" -n -o "<ENDTAGS>" -n -v "description" -n -o "<ENDITEM>" -n > $TMPFILE

# and now we process it

NEWITEM=false;
GUID="N/A"
TITLE="N/A";
LINK="N/A";
DESCRIPTION="";
TAGS="";
COUNTER=0;
ISTAG=false;

while read line
do
	if [[ $line == "<BEGINITEM>" && $NEWITEM == false ]]
	then
		NEWITEM=true;
		ISTAG=false;
	fi

	if [[ $COUNTER -eq 1 ]]
	then
		GUID=$line
	fi

	if [[ $COUNTER -eq 2 ]]
	then
		TITLE=$line
	fi

	if [[ $COUNTER -eq 3 ]]
	then
		LINK=$line
	fi

	if [[ $COUNTER -ge 4 && $line = "<BEGINTAGS>" ]]
	then
		ISTAG=true;
	fi

	if [[ "$ISTAG" = true && $line != "<BEGINTAGS>" && $line != "<ENDTAGS>" ]]
	then
		is_excluded_tag $line
		if [[ $? -eq 0 ]]
		then
			TAGS="$TAGS #${line}"
		fi
	fi

	if [[ $COUNTER -ge 4 && $line != "<ENDITEM>" && "$ISTAG" = false ]]
	then
		DESCRIPTION="${DESCRIPTION}${line}"
	fi

	if [[ $COUNTER -ge 4 && $line = "<ENDTAGS>" ]]
	then
		ISTAG=false;
	fi

	let "COUNTER += 1"

	if [[ $line == "<ENDITEM>" ]]
	then
		NEWITEM=false;
		grep "$GUID" $SEENFILE > /dev/null
		if [[ $? -eq 1 ]]
		then
			senditem "$TITLE" "$LINK" "$DESCRIPTION" "$TAGS";
			echo $GUID >> $SEENFILE
		fi
		COUNTER=0;
		DESCRIPTION="";
		TAGS="";
	fi

done < $TMPFILE

# That's all folks !
$CLIASPORABIN session close
rm -f $TMPFILE
